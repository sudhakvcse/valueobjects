import java.util.Objects;

public class Measurement {

     double value;
     UnitsOfMeasurement unit;

    private Measurement(double value, UnitsOfMeasurement unit) {
        this.value = value;
        this.unit = unit;
    }

    public static Measurement meter(double i) {
        return new Measurement(i, new UnitsOfMeasurement(Unit_Name.meter, Unit_Type.Length));
    }

    public static Measurement centimeter(double i) {
        return new Measurement(i, new UnitsOfMeasurement(Unit_Name.centimeter, Unit_Type.Length));
    }

    public static Measurement kilometer(double i) {
        return new Measurement(i, new UnitsOfMeasurement(Unit_Name.kilometer, Unit_Type.Length));
    }

    public static Measurement gram(double i) {
        return new Measurement(i, new UnitsOfMeasurement(Unit_Name.gram, Unit_Type.Weight));
    }

    public static Measurement kilogram(double i) {
        return new Measurement(i, new UnitsOfMeasurement(Unit_Name.kilogram, Unit_Type.Weight));
    }

    public static Measurement celcius(double i) {
        return new Measurement(i, new Temperature(Unit_Name.celsius, Unit_Type.Temperature));
    }

    public static Measurement fahrenheit(double i) {
        return new Measurement(i, new Temperature(Unit_Name.fahrenheit, Unit_Type.Temperature));
    }

    public static Measurement kelvin(double i) {
        return new Measurement(i, new Temperature(Unit_Name.kelvin, Unit_Type.Temperature));
    }

    @Override
    public boolean equals(Object o) {

        Measurement obj2 = ((Measurement) o);
        double value1;
        double value2;
        if (obj2.unit.unit_Type == this.unit.unit_Type) {
            value1 = unit.convertValuesToTheirBaseValue(this.value);
            value2 = obj2.unit.convertValuesToTheirBaseValue(obj2.value);
            return value1 == value2;
        } else
            throw new IllegalArgumentException("The given two measurements are not compatible");

    }

    public Measurement addTwoMeasurements(Measurement objMeasurement) {

        double value1 = unit.convertValuesToTheirBaseValue(this.value);
        double value2 = objMeasurement.unit.convertValuesToTheirBaseValue(objMeasurement.value);
        if (this.unit.unit_Type == objMeasurement.unit.unit_Type) {
            double sumValue = unit.convertValuesFromBasetoDesiredUnit(value1 + value2);
            Measurement sumOfTwoMeasures = new Measurement(sumValue, this.unit);
            return sumOfTwoMeasures;
        } else
            throw new IllegalArgumentException("The given two measurements are not compatible");
    }

    public Measurement subtractTwoMeasurements(Measurement objMeasurement) {

        double value1 = unit.convertValuesToTheirBaseValue(this.value);
        double value2 = objMeasurement.unit.convertValuesToTheirBaseValue(objMeasurement.value);
        if (this.unit.unit_Type == objMeasurement.unit.unit_Type) {
            double differenceValue = unit.convertValuesFromBasetoDesiredUnit(value1 - value2);
            Measurement sumOfTwoMeasures = new Measurement(differenceValue, this.unit);
            return sumOfTwoMeasures;
        } else
            throw new IllegalArgumentException("The given two measurements are not compatible");
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }


    enum Unit_Name {
        meter, centimeter, kilometer, gram, kilogram, celsius, kelvin, fahrenheit
    }

    enum Unit_Type {
        Length, Weight, Temperature
    }
}



