import java.util.HashMap;

public class UnitsOfMeasurement {

    public static final HashMap<String, Double> conversion = new java.util.HashMap<String, Double>();
    public static final double METER = 1.0;
    public static final double CENTI = 0.01;
    public static final double KILO = 1000.0;
    public static final double GRAM = 1.0;
    Measurement.Unit_Name unitName;
    Measurement.Unit_Type unit_Type;

    public UnitsOfMeasurement(Measurement.Unit_Name unitName, Measurement.Unit_Type unitType) {

        this.unitName = unitName;
        this.unit_Type = unitType;
        conversion.put("meter", METER);
        conversion.put("centimeter", CENTI);
        conversion.put("kilometer", KILO);
        conversion.put("gram", GRAM);
        conversion.put("kilogram", KILO);

    }

    @Override
    public boolean equals(Object o) {

        return (this.unit_Type == ((UnitsOfMeasurement) o).unit_Type);

    }

    public double convertValuesToTheirBaseValue(double value) {
        if (unit_Type.equals(Measurement.Unit_Type.Temperature)) {

            return (new Temperature(unitName, unit_Type).converttoBaseTemperature(value));

        }

        return conversion.get(unitName.name()) * value;

    }

    public double convertValuesFromBasetoDesiredUnit(double valueToBeConverted) {

        return (valueToBeConverted / conversion.get(this.unitName.name()));
    }


}


