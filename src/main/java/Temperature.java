public class Temperature extends UnitsOfMeasurement {


    public Temperature(Measurement.Unit_Name unitName, Measurement.Unit_Type unitType) {
        super(unitName, unitType);
    }


    public double converttoBaseTemperature(double value) {
        if (unitName.equals(Measurement.Unit_Name.celsius))
            return value * 1;
        else if (unitName.equals(Measurement.Unit_Name.fahrenheit))
            return Math.round((value - 32) * (0.556));
        else
            return Math.round(value - 273.15);

    }


}
