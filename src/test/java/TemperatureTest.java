import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TemperatureTest {

    @Test
    public void shouldCheckEqualityofDimensionsForTemperature() {

        assertEquals(new UnitsOfMeasurement(Measurement.Unit_Name.celsius, Measurement.Unit_Type.Temperature),
                new UnitsOfMeasurement(Measurement.Unit_Name.fahrenheit, Measurement.Unit_Type.Temperature));

    }

    @Test
    public void shouldCheckEqualityofDimensionsForTemperatureTest2() {

        assertNotEquals(new UnitsOfMeasurement(Measurement.Unit_Name.celsius, Measurement.Unit_Type.Temperature),
                new UnitsOfMeasurement(Measurement.Unit_Name.kilogram, Measurement.Unit_Type.Weight));

    }

    @Test
    public void shouldCheckConversionOfTemperature() {

        assertEquals(100, new Temperature(Measurement.Unit_Name.fahrenheit, Measurement.Unit_Type.Temperature).
                converttoBaseTemperature(212.0));
    }


}
