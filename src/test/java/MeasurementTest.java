import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MeasurementTest {


    @Test
    void shouldCheckEqualityOfMtoCM() {

        assertEquals(Measurement.meter(1),
                Measurement.centimeter(100));
    }

    @Test
    void shouldCheckEqualityOfMtoCMTest2() {

        assertEquals(Measurement.meter(10),
                Measurement.centimeter(1000));
    }

    @Test
    void shouldCheckEqualityOfMtoCMTest3() {

        assertEquals(Measurement.centimeter(10),
                Measurement.meter(0.1));
    }

    @Test
    void shouldCheckEqualityOfMtoKM() {

        assertEquals(Measurement.kilometer(10),
                Measurement.meter(10000));
    }

    @Test
    void shouldCheckEqualityOfMtoKMTest2() {

        assertEquals(Measurement.meter(10),
                Measurement.kilometer(0.01));
    }

    @Test
    void shouldCheckEqualityOfGtoKG() {

        assertEquals(Measurement.gram(4000),
                Measurement.kilogram(4));
    }

    @Test
    void shouldCheckEqualityOfGtoKGTest2() {

        assertEquals(Measurement.kilogram(0.4),
                Measurement.gram(400));
    }

    @Test
    void shouldCheckCompatibilityOfMeasurements() {
        assertThrows(IllegalArgumentException.class, () -> {
            assertEquals(Measurement.gram(4000), Measurement.meter(4));
        });

    }


    @Test
    public void shouldCheckAddMeasurements() {

        assertEquals(Measurement.centimeter(200),
                Measurement.centimeter(100).addTwoMeasurements(Measurement.meter(1)));


    }

    @Test
    public void shouldCheckAddMeasurementsTest2() {

        assertEquals(Measurement.meter(1.01),
                Measurement.meter(1).addTwoMeasurements(Measurement.centimeter(1)));


    }

    @Test
    public void shouldCheckAddMeasurementsTest3() {

        assertEquals(Measurement.gram(2000),
                Measurement.gram(1000).addTwoMeasurements(Measurement.kilogram(1)));


    }

    @Test
    public void shouldCheckAddMeasurementsTest4() {

        assertEquals(Measurement.kilogram(2),
                Measurement.kilogram(1).addTwoMeasurements(Measurement.gram(1000)));


    }

    @Test
    public void shouldCheckIllegalArgumentExceptionForIncompatibleMeasures() {

        assertThrows(IllegalArgumentException.class, () -> {
            Measurement.kilogram(1).addTwoMeasurements(Measurement.centimeter(1000));
        });
    }

    @Test
    public void shouldCheckSubtractionOfMeasurements() {

        assertEquals(Measurement.kilometer(0.9),
                Measurement.kilometer(1).subtractTwoMeasurements(Measurement.meter(100)));


    }

    @Test
    void shouldCheckEqualityOfTemperature() {

        assertEquals(Measurement.celcius(100),
                Measurement.fahrenheit(212));
    }


}
