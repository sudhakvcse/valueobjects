import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitsOfMeasurementTest {

    @Test
    public void shouldCheckDimensionsAreEqual() {

        assertEquals(new UnitsOfMeasurement(Measurement.Unit_Name.meter, Measurement.Unit_Type.Length),
                new UnitsOfMeasurement(Measurement.Unit_Name.centimeter, Measurement.Unit_Type.Length));

    }

    @Test
    public void shouldCheckDimensionsNotEqual() {

        assertNotEquals(new UnitsOfMeasurement(Measurement.Unit_Name.meter, Measurement.Unit_Type.Length),
                new UnitsOfMeasurement(Measurement.Unit_Name.gram, Measurement.Unit_Type.Weight));

    }

    @Test
    public void shouldCheckConversiontoBaseValue() {

        assertEquals(1,
                new UnitsOfMeasurement(Measurement.Unit_Name.meter, Measurement.Unit_Type.Length).convertValuesToTheirBaseValue(1));

    }

    @Test
    public void shouldCheckConversiontoBaseValueTest2() {

        assertEquals(10,
                new UnitsOfMeasurement(Measurement.Unit_Name.centimeter, Measurement.Unit_Type.Length).convertValuesToTheirBaseValue(1000));

    }

    @Test
    public void shouldCheckDimensionsEqualTest2() {

        assertEquals(new UnitsOfMeasurement(Measurement.Unit_Name.gram, Measurement.Unit_Type.Weight),
                new UnitsOfMeasurement(Measurement.Unit_Name.kilogram, Measurement.Unit_Type.Weight));

    }

    @Test
    public void shouldCheckConversiontoBaseValueTest3() {

        assertEquals(2000,
                new UnitsOfMeasurement(Measurement.Unit_Name.kilogram, Measurement.Unit_Type.Weight).convertValuesToTheirBaseValue(2));

    }

    @Test
    public void shouldCheckConversionofBaseMeasurementtoDesired() {

        assertEquals(2.0,
                new UnitsOfMeasurement(Measurement.Unit_Name.kilogram, Measurement.Unit_Type.Weight).convertValuesFromBasetoDesiredUnit(2000));


    }

}
